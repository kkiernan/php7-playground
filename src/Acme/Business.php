<?php

declare(strict_types = 1);

namespace Acme;

class Business
{
    /**
     * @var array
     */
    protected $staff = [];

    /**
     * @var string
     */
    protected $name;

    /**
     * Creates a new business instance.
     * 
     * @param string $name the name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Hires an employee.
     * 
     * @param Employee $employee the employee
     * 
     * @return self
     */
    public function hire(Employee $employee): Business
    {
        $this->staff[] = $employee;

        return $this;
    }

    /**
     * Returns the string representation of the object.
     * 
     * @return string
     */
    public function __toString(): string
    {
        return sprintf('%s has %s employee(s).', $this->name, count($this->staff));
    }
}

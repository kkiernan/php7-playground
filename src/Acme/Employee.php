<?php

declare(strict_types = 1);

namespace Acme;

class Employee
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var int
     */
    protected $salary;

    /**
     * Creates a new employee instance.
     * 
     * @param string $name
     * @param string $title
     * @param int    $salary
     */
    public function __construct(string $name, string $title, int $salary)
    {
        $this->name = $name;
        $this->title = $title;
        $this->salary = $salary;
    }

    /**
     * Gets the value of name.
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param string $name the name
     *
     * @return self
     */
    public function setName(string $name): Employee
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of title.
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Sets the value of title.
     *
     * @param string $title the title
     *
     * @return self
     */
    public function setTitle(string $title): Employee
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Gets the value of salary.
     *
     * @return int
     */
    public function getSalary(): int
    {
        return $this->salary;
    }

    /**
     * Sets the value of salary.
     *
     * @param int $salary the salary
     *
     * @return self
     */
    public function setSalary(int $salary): Employee
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Returns the string representation of the object.
     * 
     * @return string
     */
    public function __toString(): string
    {
        return sprintf(
            "%s is a %s making $%s per year.",
            $this->name,
            $this->title,
            number_format($this->salary)
        );
    }
}

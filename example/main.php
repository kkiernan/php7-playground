<?php

use Acme\Business;
use Acme\Employee;

require '../vendor/autoload.php';

$acme = new Business('Acme');
echo $acme . "\n";

$jane = new Employee('Jane Doe', 'Web Developer', 1000000);
echo $jane . "\n";

$john = new Employee('John Smith', 'Software Engineer', 1000000);
echo $john . "\n";

$acme->hire($jane)
     ->hire($john);
echo $acme;
